package com.certant.pokedexliteengrupo.pokemon.entity;

public class PokemonAuxiliar {
	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
