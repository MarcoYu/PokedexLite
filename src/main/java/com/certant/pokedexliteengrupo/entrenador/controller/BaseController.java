package com.certant.pokedexliteengrupo.entrenador.controller;

import com.certant.pokedexliteengrupo.entrenador.entity.Entrenador;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class BaseController {

    @GetMapping({"/", ""})
    public String redireccionando() {
        return "redirect:entrenador/";
    }
}
